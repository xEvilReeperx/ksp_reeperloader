﻿using System;

namespace ReeperLoader
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
    public sealed class ReeperLoadTargetAttribute : Attribute
    {

    }
}
