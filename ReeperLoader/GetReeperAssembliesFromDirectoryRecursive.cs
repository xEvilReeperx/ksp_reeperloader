﻿using System;
using System.Collections.Generic;
using System.Linq;
using ReeperAssemblyLibrary;
using strange.extensions.implicitBind;

namespace ReeperLoader
{
    [Implements(typeof(IGetReeperAssembliesFromDirectoryRecursive))]
    public class GetReeperAssembliesFromDirectoryRecursive : IGetReeperAssembliesFromDirectoryRecursive
    {
        private readonly IReeperAssemblyFactory _factory;

        public GetReeperAssembliesFromDirectoryRecursive(IReeperAssemblyFactory factory)
        {
            if (factory == null) throw new ArgumentNullException("factory");
            _factory = factory;
        }


        public IEnumerable<ReeperAssembly> Get(UrlDir gameData)
        {
            return gameData.GetFiles(UrlDir.FileType.Unknown)
                                            .Where(uf => uf.fileExtension == "reeper")
                                            .Select(uf => _factory.Create(uf));
        }
    }
}
