﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using ReeperAssemblyLibrary;
using ReeperCommon.Containers;
using ReeperCommon.Logging;
using strange.extensions.command.impl;

namespace ReeperLoader
{
// ReSharper disable once ClassNeverInstantiated.Global
    public class CommandLoadAssemblies : Command
    {
        private readonly IGetReeperAssembliesFromDirectoryRecursive _assembliesToLoad;
        private readonly UrlDir _searchDirectory;
        private readonly IReeperAssemblyLoader _assemblyLoader;
        private readonly SignalFail _failSignal;
        private readonly ILog _log;

        public CommandLoadAssemblies(
            IGetReeperAssembliesFromDirectoryRecursive assembliesToLoad, 
            UrlDir searchDirectory,
            IReeperAssemblyLoader assemblyLoader,
            SignalFail failSignal,
            ILog log)
        {
            if (assembliesToLoad == null) throw new ArgumentNullException("assembliesToLoad");
            if (searchDirectory == null) throw new ArgumentNullException("searchDirectory");
            if (assemblyLoader == null) throw new ArgumentNullException("assemblyLoader");
            if (failSignal == null) throw new ArgumentNullException("failSignal");
            if (log == null) throw new ArgumentNullException("log");

            _assembliesToLoad = assembliesToLoad;
            _searchDirectory = searchDirectory;
            _assemblyLoader = assemblyLoader;
            _failSignal = failSignal;
            _log = log;
        }


        public override void Execute()
        {
            try
            {
                var assemblies = _assembliesToLoad.Get(_searchDirectory).ToList();

                _log.Normal("Found {0} targets", assemblies.Count.ToString(CultureInfo.InvariantCulture));

                foreach (var asm in assemblies)
                    _assemblyLoader.Load(asm);

                AppDomain.CurrentDomain.GetAssemblies()
                    .ToList()
                    .ForEach(a => _log.Debug("Domain-loaded assembly: " + a));
            }
            catch (ReeperAssemblyLoadException e)
            {
                _log.Error("Exception while loading " + e.Target.File.name + ", exception was: " + e);

                _failSignal.Dispatch("Failed to load ReeperAssembly " + e.Target.File.name + "\n\nSee log for details.");
            }
            catch (Exception e)
            {
                _log.Error("Exception while loading assemblies, exception was: " + e);

                _failSignal.Dispatch("Failed to load assemblies!\n\nSee log for details.");
            }

            _log.Normal("Finished");
        }
    }
}
