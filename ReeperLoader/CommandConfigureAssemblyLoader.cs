﻿//using System;
//using ReeperAssemblyLibrary;
//using ReeperCommon.Logging;
//using strange.extensions.command.impl;
//using strange.framework.impl;

//namespace ReeperLoader
//{
//// ReSharper disable once ClassNeverInstantiated.Global
//    public class CommandConfigureAssemblyLoader : Command
//    {
//        private readonly SignalFail _failSignal;
//        private readonly ILog _log;

//        public CommandConfigureAssemblyLoader(SignalFail failSignal, ILog log)
//        {
//            if (failSignal == null) throw new ArgumentNullException("failSignal");
//            if (log == null) throw new ArgumentNullException("log");
//            _failSignal = failSignal;
//            _log = log;
//        }


//        public override void Execute()
//        {
//            try
//            {
//                injectionBinder.Bind<ReeperAssemblyLoader>().ToSingleton();

//                var loader = injectionBinder.GetInstance<ReeperAssemblyLoader>();

//                AppDomain.CurrentDomain.AssemblyResolve += loader.Resolve;

//                injectionBinder.Unbind<ReeperAssemblyLoader>();
//                injectionBinder.Bind<IReeperAssemblyLoader>().To(loader);
//            }
//            catch (BinderException e)
//            {
//                _log.Error("Binder exception while configuring assembly loader! " + e);
//                _failSignal.Dispatch("Failed to configure assembly loader due to a binding exception. See log for details.");
//            }
//            catch (Exception e)
//            {
//                _log.Error("Exception while configuring assembly loader! " + e);
//                _failSignal.Dispatch("Failed to configure assembly loader. See log for details.");
//            }
//        }
//    }
//}
