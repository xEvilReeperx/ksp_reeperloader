﻿using System;
using ReeperCommon.Logging;
using strange.extensions.command.impl;
using UnityEngine;

namespace ReeperLoader
{
// ReSharper disable once ClassNeverInstantiated.Global
    public class CommandDisplayFailureMessage : Command
    {
        private readonly string _message;
        private readonly ILog _log;


        public CommandDisplayFailureMessage(string message, ILog log)
        {
            if (message == null) throw new ArgumentNullException("message");
            if (log == null) throw new ArgumentNullException("log");

            _message = message;
            _log = log;
        }


        public override void Execute()
        {
            _log.Error("Shutting down due to: " + _message);
            PopupDialog.SpawnPopupDialog(new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f), "ReeperLoader Failure", _message, "Okay", true, HighLogic.UISkin);
        }
    }
}
