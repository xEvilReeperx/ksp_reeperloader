﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ReeperCommon.Logging;
using strange.extensions.command.impl;
using strange.extensions.context.api;
using strange.extensions.injector;
using UnityEngine;

namespace ReeperLoader
{
    public class CommandShutdown : Command
    {
        private readonly GameObject _gameObject;
        private readonly ILog _log;

        public CommandShutdown([Name(ContextKeys.CONTEXT_VIEW)] GameObject gameObject, ILog log)
        {
            if (gameObject == null) throw new ArgumentNullException("gameObject");
            if (log == null) throw new ArgumentNullException("log");

            _gameObject = gameObject;
            _log = log;
        }

        public override void Execute()
        {
            _log.Normal("Shutting down");
            UnityEngine.Object.Destroy(_gameObject);
        }
    }
}
