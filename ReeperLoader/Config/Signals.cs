﻿using strange.extensions.signal.impl;

namespace ReeperLoader.Config
{
    public class SignalStart : Signal
    {
    }

    public class SignalShutdown : Signal
    {
        
    }
}
