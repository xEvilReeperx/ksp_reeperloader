﻿using System;
using System.Collections;
using strange.extensions.context.impl;
using UnityEngine;

namespace ReeperLoader.Config
{
    [KSPAddon(KSPAddon.Startup.Instantly, true)]
// ReSharper disable once UnusedMember.Global
    public class BootstrapCore : ContextView
    {
// ReSharper disable once UnusedMember.Local
        private void Start()
        {
            try
            {

                context = new CoreContext(this);
                context.Launch();
            }
            catch (Exception ex)
            {
                Debug.LogException(ex);
                PopupDialog.SpawnPopupDialog(new Vector2(0.5f, 0.5f), new Vector2(0.5f, 0.5f), "ReeperLoader Error",
                    string.Format("A serious exception has occurred. See log for details.\n\n{0}", ex.Message), "OK", true, HighLogic.UISkin);
            }
            
        }
    }
}
