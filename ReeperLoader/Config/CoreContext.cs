﻿using ReeperAssemblyLibrary;
using ReeperCommon.Logging;
using strange.extensions.command.api;
using strange.extensions.command.impl;
using strange.extensions.context.api;
using strange.extensions.context.impl;
using UnityEngine;

namespace ReeperLoader.Config
{
    public class CoreContext : MVCSContext
    {
        public CoreContext(MonoBehaviour view)
            : base(view, ContextStartupFlags.MANUAL_LAUNCH)
        {
            
        }


        protected override void mapBindings()
        {
            base.mapBindings();

            implicitBinder.ScanForAnnotatedClasses(new [] { "ReeperLoader" });

            injectionBinder.Bind<ILog>().To(new DebugLog("ReeperLoader"));

            injectionBinder.Bind<UrlDir>().To(GameDatabase.Instance.root);
            injectionBinder.Bind<IReeperAssemblyFactory>().To(new ReeperAssemblyFactory("reeper", "mdb"));
            injectionBinder.Bind<IGetReeperAssembliesFromDirectoryRecursive>()
                .To<GetReeperAssembliesFromDirectoryRecursive>().ToSingleton();

            injectionBinder.Bind<ILoadedAssemblyInstaller>().To<LoadedAssemblyInstaller>().ToSingleton();
            injectionBinder.Bind<IRawAssemblyDataFactory>().To<RawAssemblyDataFactory>().ToSingleton();
            injectionBinder.Bind<IReeperAssemblyResolver>().To<ReeperAssemblyResolver>().ToSingleton();
            injectionBinder.Bind<IReeperAssemblyLoader>().To<ReeperAssemblyLoader>().ToSingleton();

            MapCommands();
        }


        protected override void addCoreComponents()
        {
            base.addCoreComponents();
            injectionBinder.Unbind<ICommandBinder>();
            injectionBinder.Bind<ICommandBinder>().To<SignalCommandBinder>().ToSingleton();
        }


        private void MapCommands()
        {
            commandBinder.Bind<SignalStart>()
                .To<CommandLaunch>()
                .To<CommandConfigureAssemblyResolvers>()
                //.To<CommandConfigureAssemblyLoader>()
                .To<CommandLoadAssemblies>()
                .To<CommandRunLoadTargets>()
                .To<CommandInitiateShutdown>()
                .Once();

            commandBinder.Bind<SignalFail>()
                .To<CommandDisplayFailureMessage>()
                .To<CommandInitiateShutdown>();

            commandBinder.Bind<SignalShutdown>()
                .To<CommandShutdown>();
        }


        public override void Launch()
        {
            base.Launch();
            injectionBinder.GetInstance<SignalStart>().Dispatch();
        }
    }
}
