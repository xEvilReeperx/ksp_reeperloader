﻿using System;
using Mono.Cecil;
using ReeperCommon.Logging;
using strange.extensions.command.impl;

namespace ReeperLoader.Config
{
// ReSharper disable once ClassNeverInstantiated.Global
    public class CommandLaunch : Command
    {
        private readonly ILog _log;

        public CommandLaunch(ILog log)
        {
            if (log == null) throw new ArgumentNullException("log");
            _log = log;
        }


        public override void Execute()
        {
            _log.Warning("Launching...");
            _log.Verbose("Mono.Cecil version: " + typeof (AssemblyDefinition).Assembly.FullName);
        }
    }
}
