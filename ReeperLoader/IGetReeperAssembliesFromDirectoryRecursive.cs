﻿using System.Collections;
using System.Collections.Generic;
using ReeperAssemblyLibrary;

namespace ReeperLoader
{
    public interface IGetReeperAssembliesFromDirectoryRecursive
    {
        IEnumerable<ReeperAssembly> Get(UrlDir gameData);
    }
}
