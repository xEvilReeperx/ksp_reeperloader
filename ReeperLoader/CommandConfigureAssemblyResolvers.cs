﻿using System;
using System.Linq;
using Mono.Cecil;
using ReeperAssemblyLibrary;
using ReeperCommon.Logging;
using strange.extensions.command.impl;

namespace ReeperLoader
{
// ReSharper disable once ClassNeverInstantiated.Global
    public class CommandConfigureAssemblyResolvers : Command
    {
        private readonly UrlDir _gameData;
        private readonly SignalFail _failSignal;
        private readonly ILog _log;

        public CommandConfigureAssemblyResolvers(
            UrlDir gameData,
            SignalFail failSignal,
            ILog log)
        {
            if (gameData == null) throw new ArgumentNullException("gameData");
            if (failSignal == null) throw new ArgumentNullException("failSignal");
            if (log == null) throw new ArgumentNullException("log");

            _gameData = gameData;
            _failSignal = failSignal;
            _log = log;
        }

        public override void Execute()
        {
            try
            {
                _log.Normal("Configuring assembly resolver...");

                var resolver = new DefaultAssemblyResolver(); // this resolves cecil definitions and references

                foreach (var dir in _gameData.AllDirectories
                    .Where(dir => !string.IsNullOrEmpty(dir.path))
                    .Select(dir => dir.path)
                    .Distinct())

                    resolver.AddSearchDirectory(dir);

                injectionBinder.Bind<BaseAssemblyResolver>().To(resolver);

                AppDomain.CurrentDomain.AssemblyResolve +=
                    injectionBinder.GetInstance<IReeperAssemblyResolver>().Resolve; // this will resolve any references the target may have to other ReeperAssemblies already in memory

                _log.Normal("Assembly resolvers successfully configured");
            }
            catch (Exception e)
            {
                _log.Error("Exception while configuring assembly resolvers! " + e);
                _failSignal.Dispatch("Failed to configure assembly resolvers. See log for details.");
            }
        }
    }
}
