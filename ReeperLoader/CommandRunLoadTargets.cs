﻿using System;
using System.Collections.Generic;
using System.Linq;
using ReeperCommon.Logging;
using strange.extensions.command.impl;
using UnityEngine;

namespace ReeperLoader
{
// ReSharper disable once ClassNeverInstantiated.Global
    public class CommandRunLoadTargets : Command
    {
        private readonly ILog _log;

        public CommandRunLoadTargets(ILog log)
        {
            if (log == null) throw new ArgumentNullException("log");

            _log = log;
        }


        public override void Execute()
        {
            _log.Normal("Launching targets...");

// ReSharper disable once NotAccessedVariable
            GameObject go;

            foreach (var target in AssemblyLoader.loadedAssemblies.SelectMany(GetLoadTargets))
            {
                if (!target.IsSubclassOf(typeof (MonoBehaviour)))
                {
                    _log.Error("Cannot launch target: " + target.FullName + " -- does not subclass MonoBehaviour");
                    continue;
                }

                _log.Debug("Launching " + target.FullName);

// ReSharper disable once RedundantAssignment
                go = new GameObject(target.Name, target);
            }

            _log.Normal("Finished launching targets.");
        }

        private static IEnumerable<Type> GetLoadTargets(AssemblyLoader.LoadedAssembly la)
        {
            return la.assembly.GetTypes()
                .Where(t => !t.IsAbstract && t.GetCustomAttributes(typeof (ReeperLoadTargetAttribute), true).Any());
        }
    }
}
