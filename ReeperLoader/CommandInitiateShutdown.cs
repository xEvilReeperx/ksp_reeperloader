﻿using System;
using ReeperLoader.Config;
using strange.extensions.command.impl;

namespace ReeperLoader
{
// ReSharper disable once ClassNeverInstantiated.Global
    public class CommandInitiateShutdown : Command
    {
        private readonly SignalShutdown _signal;

        public CommandInitiateShutdown(SignalShutdown signal)
        {
            if (signal == null) throw new ArgumentNullException("signal");
            _signal = signal;
        }


        public override void Execute()
        {
            _signal.Dispatch();
        }
    }
}
